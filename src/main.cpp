// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "game.h"

int main(int argc, char *argv[])
{
  Game().start();
  return 0;
}
