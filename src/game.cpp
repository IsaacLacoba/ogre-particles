// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "game.h"

Game::Game() {
  scene_ = std::make_shared<Scene>();
  input_ = std::make_shared<EventListener>(scene_->window_);
}

Game::~Game() {
}

void
Game::start() {
  register_hooks();
  game_loop();
}

void
Game::game_loop() {
  float delta_time;

  while(!input_->exit_) {
    delta_time += timer_.get_delta_time();
    input_->check_events();
    if(delta_time >= 0.017) {
      input_->capture();
      scene_->render_one_frame();
      delta_time = 0.f;
    }
  }
}

void
Game::register_hooks() {
  input_->add_hook({std::make_pair(OIS::KC_ESCAPE, true)}, EventType::menu,
                   std::bind(&EventListener::shutdown, input_));
  input_->add_hook({std::make_pair(OIS::KC_1, true)}, EventType::menu,
                   std::bind(&Game::create_particle_system, this,
                             "sun", "Space/Sun"));
  input_->add_hook({std::make_pair(OIS::KC_1, false)}, EventType::menu,
                   std::bind(&Scene::remove_child, scene_, "", "sun"));
  input_->add_hook({std::make_pair(OIS::KC_2, true)}, EventType::menu,
                   std::bind(&Game::create_particle_system, this,
                             "fountain", "Examples/PurpleFountain" ));
  input_->add_hook({std::make_pair(OIS::KC_2, false)}, EventType::menu,
                   std::bind(&Scene::remove_child, scene_, "", "fountain"));
}

void
Game::create_particle_system(std::string name, std::string particle) {
  scene_->create_particle(name, particle);
}
