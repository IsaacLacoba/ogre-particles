#ifndef GAME_H
#define GAME_H
#include <memory>

#include "scene.h"
#include "input.h"
#include "timer.h"

class Game {
  Timer timer_;

  Scene::shared scene_;
  EventListener::shared input_;

public:
  Game();
  virtual ~Game();

  void start();

private:
  void game_loop();
  void create_ground(Ogre::Vector3 position, std::string name,
                     std::string mesh, Ogre::Degree angle);
  void create_particle_system(std::string name, std::string particle);
  void register_hooks();
};


#endif
